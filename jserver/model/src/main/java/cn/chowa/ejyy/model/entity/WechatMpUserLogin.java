package cn.chowa.ejyy.model.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.DynamicUpdate;

import javax.persistence.*;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Entity
@Table(name = "ejyy_wechat_mp_user_login")
@DynamicUpdate
public class WechatMpUserLogin {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    private long wechat_mp_user_id;

    private String ip;

    private String brand;

    private String model;

    private String system;

    private String platform;

    private long login_at;

}
