package cn.chowa.ejyy.dao;

import cn.chowa.ejyy.model.entity.UserCarOperateLog;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserCarOperateLogRepository extends JpaRepository<UserCarOperateLog, Long> {
}
