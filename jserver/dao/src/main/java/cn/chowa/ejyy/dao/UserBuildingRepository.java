package cn.chowa.ejyy.dao;

import cn.chowa.ejyy.model.entity.UserBuilding;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserBuildingRepository extends JpaRepository<UserBuilding, Long> {

}
